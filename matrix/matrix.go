package matrix

import (
        "fmt"
)

type Matrix struct {
	body [][]int
	r  []int //row{read,write}
	c  []int //col{read,write}
	s  int	 //really size ind
	l  int	 //submatrix low size ind
	h  int	 //submatrix hight size ind
}

func NewMatrix(n int) *Matrix {
	if n < 2 {
		return nil
	}
	return &Matrix{func() [][]int {
		b := make([][]int, n)
		for i := range b {
			b[i] = make([]int, n)
		}
		return b
	}(),
		[]int{0, 0},
		[]int{0, 0},
		n-1,
		0,
		n-1,
	}
}

func writeCellText(m *Matrix, n int) bool {
//	if m.r[1] == m.s && m.c[1] > m.s {
	if m.c[1] > m.s {
		return false
	}
	m.body[m.r[1]][m.c[1]] = n
	if m.c[1] < m.s {
		m.c[1]++
	} else if m.r[1] == m.s&& m.c[1] == m.s {
		m.c[1]++
	} else {
		m.r[1]++
		m.c[1] = 0
	}
	return true
}

func writeCellClockWise(m *Matrix, n int) bool {
	if m.h == m.l {
		return false
	}
	m.body[m.r[1]][m.c[1]] = n

	if m.r[1] == m.l {
		if m.c[1] != m.h { // >
			if m.h - m.l == 1 && m.s % 2 == 0 { //ditect end odd matrix
				m.h--
			}
			m.c[1]++
		} else {	   // v 
			m.r[1]++
		}
	} else if m.c[1] == m.h {
		if m.r[1] != m.h { // v
			m.r[1]++
		} else {
			m.c[1]--   // <
		}
	} else if m.r[1] == m.h {
		if m.c[1] != m.l { // <
			m.c[1]--
		} else {	   // ^
			if m.h - m.l == 1 {  // detect end even matrix
				m.l++
			}
			m.r[1]--
		}
	} else if m.c[1] == m.l {
		if m.r[1] != m.l + 1 { // ^
			m.r[1]--
		} else {	       // detect full circle
			m.c[1]++       // >
			m.l++
			if m.h - m.l != 1 {   // detect last but one odd matrix
				m.h--
			}
		}
	}
	return true
}

func (m *Matrix) WriteText(d ...int) bool {
	s := []int(d)
	if len(s) == 0 {
		return true
	}

	if m.h != 0 {	// test empty or way write yet
		if m.r[1] + m.c[1] == 0 {
			m.h = m.l
		} else {
			return false
		}
	}
	if ok := writeCellText(m, s[0]); ok {
		return m.WriteText(s[1:]...)
	}
	return false
}

func (m *Matrix) WriteClockWise(d ...int) bool {
	s := []int(d)
	if len(s) == 0 {
		return true
	}
	if ok := writeCellClockWise(m, s[0]); ok {
		return m.WriteClockWise(s[1:]...)
	}
	return false
}

func (m *Matrix) ReWind() {
	m.r[0] = 0
	m.c[0] = 0
}

////func (m *RWMatrix) ReadCellClockWise() (int, bool) {
////	if m.r == m.s && m.c > m.s {
////	}
////	return 0, false
////}

func readCellText(m *Matrix) (int, bool) {
	if m.r[0] == m.s && m.c[0] > m.s {
		return 0, false
	}
	n := m.body[m.r[0]][m.c[0]]
	if m.c[0] < m.s {
		m.c[0]++
	} else if m.r[0] == m.s && m.c[0] == m.s {
		m.c[0]++
	} else {
		m.r[0]++
		m.c[0] = 0
	}
	return n, true
}

func (m *Matrix) ReadText() {
	if v, ok := readCellText(m); ok {
		fmt.Printf("%-4d", v)
		if m.c[0] == 0 || m.c[0] > m.s {
			fmt.Println("")
		}
		m.ReadText()
	}
}

func (m *Matrix) ReadMeta() {
	if m.s == m.l && m.s == m.h {
		fmt.Print("TextWiseWay ")
	} else {
		fmt.Print("ClockWiseWay ")
	}
	fmt.Println(m.r, m.c, m.s, m.l, m.h)
}

func GenSeq(start, n int) []int {
	s := make([]int, n)
	for i := range s {
		s[i] = start
		start++
	}
	return s
}
